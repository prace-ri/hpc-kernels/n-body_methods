
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Evghenii Gaburov <evghenii.gaburov@surfsara.nl>
#
# ==================================================================================================

# CMake project
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)
project("4_nbody" NONE)
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/common.cmake)

# ==================================================================================================

# Dwarf 4: N-body methods
message("--------------------")
message("Dwarf 4: N-body methods:")
message("--------------------")
set(DWARF_PREFIX 4_nbody) # The prefix of the name of the binaries produced

# Add the examples
add_subdirectory(hermite4)
add_subdirectory(dynamic_sparse_data_exchange)
add_subdirectory(naive)
add_subdirectory(bhtree_mpi)
add_subdirectory(bhtree_pthread)

#
## BHTree requies gcc compiler, so we create it as a separate projectg
#
set(bhtree_as_external False)
if (${bhtree_as_external})
  include(ExternalProject)
  if ("$ENV{GNU_CXX}" STREQUAL "")
    message("## WARNING: GNU_CXX is not set, using default C++ compiler for ${DWARF_PREFIX}_bhtree")
    ExternalProject_Add(${DWARF_PREFIX}_bhtree 
      BINARY_DIR bhtree
      SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}tree
      INSTALL_COMMAND ""
      )
  else()
    ExternalProject_Add(${DWARF_PREFIX}_bhtree 
      BINARY_DIR bhtree
      SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}tree
      CMAKE_ARGS -DCMAKE_CXX_COMPILER=$ENV{GNU_CXX}
      INSTALL_COMMAND ""
      )
  endif()
else()
  add_subdirectory(bhtree)
endif()

#add_subdirectory(fmm)
#add_subdirectory(sph)
#add_subdirectory(md)

# ==================================================================================================




