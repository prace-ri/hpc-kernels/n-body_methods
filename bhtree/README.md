=======
README
=======

# 1. Code sample name
BHTree

# 2. Description of the code sample package
This example demonstrates a multi-core implementation of the Barnes-Hut nbody algorithm.

Additional pre-requisites:
* OpenMP

# 3. Release date
25 July 2015

# 4. Version history 
1.0

# 5. Contributor (s) / Maintainer(s) 
Evghenii Gaburov <evghenii.gaburov@surfsara.nl>

# 6. Copyright / License of the code sample
Apache 2.0

# 7. Language(s) 
C++ (C++11 is required)

# 8. Parallelisation Implementation(s)
multi-core CPU

# 9. Level of the code sample complexity 
Source-level implementation

# 10. Instructions on how to compile the code
Uses the CodeVault CMake infrastructure, see main README.md. 2 executables will be generated, a serial one and a OpenMP multi-core one.

# 11. Instructions on how to run the code
Run the executable with 2 command-line parameters, the number of bodies and the threshold theta value.
./bhtree <nbodies> <theta_value>
If the parameters are not provided it will default to 10240 bodies with theta=.75

# 12. Sample input(s)
Input-data is generated automatically when running the program.

# 13. Sample output(s)

