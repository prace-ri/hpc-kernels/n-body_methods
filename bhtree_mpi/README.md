=======
README
=======

# 1. Code sample name
4_nbody_bhtree_mpi

# 2. Description of the code sample package
This example demonstrates an MPI  implementation of the Barnes-Hut nbody algorithm.

Additional pre-requisites:
* MPI

# 3. Release date
28 October 2016

# 4. Version history 
1.0 first release
1.1 extensive refactoring with C++ 14

# 5. Contributor (s) / Maintainer(s) 
Paul Heinzlreiter <paul.heinzlreiter@risc-software.at>
Thomas Steinreiter <thomas.steinreiter@risc-software.at>

# 6. Copyright / License of the code sample
Apache 2.0

# 7. Language(s) 
C++ 14

# 8. Parallelisation Implementation(s)
MPI 3.0

# 9. Level of the code sample complexity 
Intermediate

# 10. Instructions on how to compile the code
cmake <bhtree_mpi directory>
make

# 11. Instructions on how to run the code
mpirun -np <number of processes> 4_nbody_bhtree_mpi <bhtree_mpi directory>/src/data/<input_file>

# 12. Sample input(s)
Sample input data files are provided in <bhtree_mpi directory>/src/data
They are partially taken from
http://bima.astro.umd.edu/nemo/archive/#dubinski

# 13. Sample output(s)

	rank 16: initialize...
	rank 17: initialize...
	rank 18: initialize...
	rank 19: initialize...
	rank 20: initialize...
	rank 21: initialize...
	rank 22: initialize...
	rank 23: initialize...
	rank 24: initialize...
	rank 25: initialize...
	rank 0: initialize...
	rank 0: load particles ...
	
	...


