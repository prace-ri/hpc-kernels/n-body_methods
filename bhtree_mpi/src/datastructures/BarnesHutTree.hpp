#ifndef BARNES_HUT_TREE_HPP
#define BARNES_HUT_TREE_HPP

#include "Tree.hpp"
#include "Box.hpp"

namespace nbody {
	class Node;

	class BarnesHutTree : public Tree {
	protected:
		static std::array<Box, 8> splitBB(const Node* node);
		static bool splitNode(Node* current);
		void update();
		void init(std::vector<Body> bodies, Box domain);
		static void split(Node* current);
	public:
		explicit BarnesHutTree(std::size_t parallelId);
		~BarnesHutTree() override = default;
		void build(std::vector<Body> bodies) override;
		void build(std::vector<Body> bodies, Box domain) override;
		void mergeLET(const std::vector<Body>& bodies);
		std::size_t numberOfChildren() const override;
		static void splitSubtree(Node* root);
	};
} // namespace nbody

#endif
