#ifndef BOX_HPP
#define BOX_HPP

#include <vector>
#include <array>
#include <Body.hpp>

namespace nbody {
	using namespace std;

	typedef struct _Box {
		array<double, 3> min;
		array<double, 3> max;
	} Box;

	void initBox(Box& box);
	void extendToCube(Box& box);
	void extendForBodies(Box& box, vector<Body> bodies);
	vector<Body> extractBodies(Box box, vector<Body>& bodies);
	vector<Body> copyBodies(Box box, vector<Body> bodies);
	bool isContained(Body body, Box box);
	bool isContained(Box inner, Box outer);
	double volume(Box box);
	double maxSidelength(Box box);
	bool isCorrectBox(Box box);
	bool isValid(Box box);
	void printBB(int parallelId, Box box);
	bool overlapsSphere(Box box, double* sphereCenter, double sphereRadius);
	double distanceToPosition(Box box, double* position);
	double distanceToBox(Box box1, Box box2);
	vector<Box> octreeSplit(Box box);
	vector<Box> splitLongestSide(Box box);
	bool contained(Box box, array<double, 3> position);
	void extend(Box& box, Box extender);
	void extend(Box& box, Body extender);
}

#endif
